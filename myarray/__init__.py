from myarray.basearray import BaseArray

a = BaseArray((5,), dtype=int)
a[1] = 1
a[2] = 4
a[3] = 16
a[4] = 64
a[5] = 256


b = 0

if __name__ == "__main__":
    for v in a:
        print(v)
        b += 1
    print(b)
