from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray


class TestBaseArray(TestCase):

    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[1], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[1], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[1], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[1] = 1
        self.assertEqual(1, a[1])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[1, 1] = 1
        a[2, 1] = 1
        self.assertEqual(1, a[1, 1])
        self.assertEqual(1, a[2, 1])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[4, 1] = 0
        self.assertEqual(('indice (4, 1), axis 0 out of bounds [1, 2]',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[2] = 0
        self.assertEqual(('indice (2,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[1, 1, 4])
        self.assertEqual(13, a[2, 1, 2])

        # TODO enter invalid type

    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[1] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((1, 1), 0),
                                ((2, 2), 3),
                                ((4, 1), 6),
                                ((4, 2), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((1, 1, 1), 0),
                                ((2, 2, 1), 12),
                                ((1, 1, 4), 3),
                                ((2, 2, 3), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(2))
        self.assertTrue(ndarray._is_valid_indice((2, 1)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([2]))
        self.assertFalse(ndarray._is_valid_indice(2.0))
        self.assertFalse(ndarray._is_valid_indice((2, 3.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 1, 2),
                        (2, 2, 2),
                        (2, 3, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((1, 1, 2), (1, 1, 3), (1, 1, 4),
                        (1, 2, 2), (1, 2, 3), (1, 2, 4),
                        (1, 3, 2), (1, 3, 3), (1, 3, 4),
                        (3, 1, 2), (3, 1, 3), (3, 1, 4),
                        (3, 2, 2), (3, 2, 3), (3, 2, 4),
                        (3, 3, 2), (3, 3, 3), (3, 3, 4),
                        (5, 1, 2), (5, 1, 3), (5, 1, 4),
                        (5, 2, 2), (5, 2, 3), (5, 2, 4),
                        (5, 3, 2), (5, 3, 3), (5, 3, 4),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

    """
        /\ 
        ||
    Obstoječi testi
    ------------------------
    Moji testi
        ||    
        \/ 
    """

    def test_to_string_3D(self):
        expected = "       1      -2\n  3.3333       4\n\n     5.5      -6\n       7  -8.902\n\n"
        test_array = BaseArray((2, 2, 2), dtype=float, data=[1, -2, 3.3333, 4, 5.5, -6, 7, -8.902])
        self.assertEqual(expected, test_array.__to_string__())

    def test_to_string_2D(self):
        expected = "       1      -2\n  3.3333       4\n"
        test_array = BaseArray((2, 2), dtype=float, data=[1, -2, 3.3333, 4])
        self.assertEqual(expected, test_array.__to_string__())

    def test_search_1D_none(self):
        expected = "() - Warning no matches found!"
        test_array = BaseArray((3,), dtype=float, data=tuple(range(3)))
        self.assertEqual(expected, test_array.__search_for_elements__(-1))

    def test_search_1D_single(self):
        expected = "((1))"
        test_array = BaseArray((3,), dtype=float, data=tuple(range(3)))
        self.assertEqual(expected, test_array.__search_for_elements__(0))

    def test_search_1D_multiple(self):
        expected = "((1), (2))"
        test_array = BaseArray((3,), dtype=float, data=tuple(range(3)))
        test_array[2] = 0
        self.assertEqual(expected, test_array.__search_for_elements__(0))

    def test_search_2D_none(self):
        expected = "() - Warning no matches found!"
        test_array = BaseArray((3, 3), dtype=float, data=tuple(range(9)))
        self.assertEqual(expected, test_array.__search_for_elements__(-1))

    def test_search_2D_single(self):
        expected = "((1, 1))"
        test_array = BaseArray((3, 3), dtype=float, data=tuple(range(9)))
        self.assertEqual(expected, test_array.__search_for_elements__(0))

    def test_search_2D_multiple(self):
        expected = "((1, 1), (2, 1))"
        test_array = BaseArray((3, 3), dtype=float, data=tuple(range(9)))
        test_array[2, 1] = 0
        self.assertEqual(expected, test_array.__search_for_elements__(0))

    def test_search_3D_none(self):
        expected = "() - Warning no matches found!"
        test_array = BaseArray((3, 3, 3), dtype=float, data=tuple(range(27)))
        self.assertEqual(expected, test_array.__search_for_elements__(-1))

    def test_search_3D_single(self):
        expected = "((1, 1, 1))"
        test_array = BaseArray((3, 3, 3), dtype=float, data=tuple(range(27)))
        self.assertEqual(expected, test_array.__search_for_elements__(0))

    def test_search_3D_multiple(self):
        expected = "((1, 1, 1), (2, 1, 2))"
        test_array = BaseArray((3, 3, 3), dtype=float, data=tuple(range(27)))
        test_array[2, 1, 2] = 0
        self.assertEqual(expected, test_array.__search_for_elements__(0))

    def test_merge_sort_1d(self):
        expected = BaseArray((3,), dtype=float, data=(0, 1, 2))
        tested = BaseArray((3,), dtype=float, data=(2, 0, 1))
        tested.__sort__(0)
        self.assertEqual(tested._equals(expected), True)

    def test_merge_sort_2d_row(self):
        expected = BaseArray((2, 3), dtype=float, data=(0, 1, 2, 3, 4, 5))
        tested = BaseArray((2, 3), dtype=float, data=(2, 0, 1, 5, 3, 4))
        tested.__sort__(0)
        self.assertEqual(tested._equals(expected), True)

    def test_merge_sort_2d_column(self):
        expected = BaseArray((3, 2), dtype=float, data=(0, 1, 2, 3, 4, 5))
        tested = BaseArray((3, 2), dtype=float, data=(2, 5, 0, 1, 4, 3))
        tested.__sort__(1)
        self.assertEqual(tested._equals(expected), True)

    def test_mul_skalar_int(self):
        expected = BaseArray((3, 2), dtype=int, data=(2, 4, 6, 8, 10, 12))
        tested = BaseArray((3, 2), dtype=int, data=(1, 2, 3, 4, 5, 6))
        tested = tested * 2
        self.assertEqual(expected.dtype, tested.dtype)
        self.assertEqual(tested._equals(expected), True)

    def test_mul_skalar_float(self):
        expected = BaseArray((3, 2), dtype=float, data=(2.1, 4.2, 6.3, 8.4, 10.5, 12.6))
        tested = BaseArray((3, 2), dtype=int, data=(1, 2, 3, 4, 5, 6))
        tested = tested * 2.1
        self.assertEqual(expected.dtype, tested.dtype)
        self.assertEqual(tested._equals(expected), True)

    def test_mul_matrix(self):
        expected = BaseArray((2, 2), dtype=int, data=(11, 7, 7, 5))
        a = BaseArray((2, 2), dtype=int, data=(4, 1, 2, 1))
        b = BaseArray((2, 2), dtype=int, data=(2, 1, 3, 3))
        tested = a * b
        self.assertEqual(expected.dtype, tested.dtype)
        self.assertEqual(tested._equals(expected), True)

    def test_mul_matrix_wrong(self):
        a = BaseArray((2, 5), dtype=int)
        b = BaseArray((2, 2), dtype=int)
        try:
            a * b
            self.assertEqual(True, False)
        except:
            self.assertEqual(True, True)

    def test_add_matrix(self):
        expected = BaseArray((2, 2), dtype=int, data=(6, 2, 5, 4))
        a = BaseArray((2, 2), dtype=int, data=(4, 1, 2, 1))
        b = BaseArray((2, 2), dtype=int, data=(2, 1, 3, 3))
        tested = a + b
        self.assertEqual(expected.dtype, tested.dtype)
        self.assertEqual(tested._equals(expected), True)

    def test_sub_matrix(self):
        expected = BaseArray((2, 2), dtype=int, data=(2, 0, -1, -2))
        a = BaseArray((2, 2), dtype=int, data=(4, 1, 2, 1))
        b = BaseArray((2, 2), dtype=int, data=(2, 1, 3, 3))
        tested = a - b
        self.assertEqual(expected.dtype, tested.dtype)
        self.assertEqual(tested._equals(expected), True)

    def test_pow(self):
        expected = BaseArray((2, 2), dtype=int, data=(5, 4, 4, 5))
        a = BaseArray((2, 2), dtype=int, data=(1, 2, 2, 1))
        tested = a.__pow__(2)
        self.assertEqual(expected.dtype, tested.dtype)
        self.assertEqual(tested._equals(expected), True)

    def test_log(self):
        expected = BaseArray((2, 2), dtype=float, data=(1, 2, 3, 4))
        a = BaseArray((2, 2), dtype=int, data=(2, 4, 8, 16))
        tested = a.__log__(2)
        self.assertEqual(expected.dtype, tested.dtype)
        self.assertEqual(tested._equals(expected), True)

    pass
