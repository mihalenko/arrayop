# pylint: disable=line-too-long
# pylint: disable=invalid-name

from typing import Tuple
from typing import List
from typing import Union
import array
import math


class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        _check_shape(shape)
        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n_elements = 1
        for s in self.__shape:
            n_elements *= s

        if data is None:
            if dtype == int:
                self.__data = array.array('i', [0]*n_elements)
            elif dtype == float:
                self.__data = array.array('f', [0]*n_elements)
        else:
            if len(data) != n_elements:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 1 or ind[ax] > self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds [1, {:}]'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    def __to_string__(self):
        max_len = 0
        for v in self:
            ln = str(v).__len__()
            if ln > max_len:
                max_len = ln
        data_amount = 1
        for i in self.shape:
            data_amount *= i
        org_shape = BaseArray((self.shape.__len__(),), dtype=int)
        shape = BaseArray((self.shape.__len__(),), dtype=int)
        for i in range(self.shape.__len__()):
            org_shape[i+1] = self.shape[i]
            shape[i+1] = self.shape[i]

        def check_shape():
            for j in range(shape.shape[0], 0, -1):
                if j > 1:
                    if shape[j] == 0:
                        shape[j] = org_shape[j]
                        shape[j - 1] -= 1
                        if j == shape.shape[0] - 1:
                            return "\n" + check_shape()
                        else:
                            return check_shape()
            return ""

        last_idx = shape.shape[0]
        out_str = ""
        p_str = ""
        for i in self:
            next_num = str(i)
            p_str += ((max_len - next_num.__len__() + 2) * " ") + next_num
            shape[last_idx] -= 1
            if shape[last_idx] == 0:
                out_str += p_str + "\n"
                p_str = ""
                out_str += check_shape()
        return out_str

    def __search_for_elements__(self, wanted):
        counter = 0;
        retString = "("
        s = BaseArray((self.shape.__len__(),), dtype=int)
        for i in range(self.shape.__len__() - 1, -1, -1):
            if i == self.shape.__len__() - 1:
                s[i+1] = 1
            else:
                s[i+1] = s[i + 2] * self.shape[i + 1]

        def conver_to_pos(variab):
            pos = "("
            t = variab
            for idx in s:
                pos += str(math.floor(t / idx)+1) + ", "
                t = t % idx
            return pos[:-2] + "), "

        matches = 0;
        for var in self:
            if var == wanted:
                retString += conver_to_pos(counter)
                matches += 1
            counter += 1

        if matches == 0:
            return "() - Warning no matches found!"
        return retString[:-2] + ")"

    def __sort__(self, mode: int):
        """
                Sorts this BaseArray.
                :param self: NaN
                :param mode: 0 for rows, 1 for columns
                :return: does not return
                """
        def ms1d(arr: list):
            if arr.__len__() == 1:
                return arr
            else:
                mid = math.ceil(arr.__len__()/2)
                out = []
                m1 = ms1d(arr[:mid])
                m2 = ms1d(arr[mid:])
                i = 0
                j = 0
                while i + j < arr.__len__():
                    if m1.__len__() == i:
                        out.append(m2[j])
                        j += 1
                    elif m2.__len__() == j:
                        out.append(m1[i])
                        i += 1
                    elif m1[i] <= m2[j]:
                        out.append(m1[i])
                        i += 1
                    else:
                        out.append(m2[j])
                        j += 1
                return out
        if self.shape.__len__() == 1:
            self.__data = ms1d(self.__data)
        elif self.shape.__len__() == 2:
            if mode == 0: #row sort
                new_data = []
                start = 0
                end = self.shape[1]
                for row in range(self.shape[0]):
                    tmp = ms1d(self.__data[start:end])
                    for tmp_i in tmp:
                        new_data.append(tmp_i)
                    start += self.shape[1]
                    end += self.shape[1]
                self.__data = new_data
            else: #column sort
                for col in range(self.shape[1]):
                    column = []
                    for row in range(self.shape[0]):
                        column.append(self.__data[row*self.shape[1]+col])
                    column = ms1d(column)
                    for row in range(self.shape[0]):
                        self.__data[row*self.shape[1]+col] = column[row]
        else:
            print("Can only sort 1D or 2D BaseArrays!")

    def __mul__(self, other):
        if type(other) == int:
            new_data = []
            for i in self.__data:
                new_data.append(i * other)
            return BaseArray(self.shape, self.dtype, new_data)
        elif type(other) == float:
            new_data = []
            for i in self.__data:
                new_data.append(i * other)
            return BaseArray(self.shape, dtype=float, data=new_data)
        elif type(other) == BaseArray:
            if self.shape.__len__() == other.shape.__len__():
                if self.shape.__len__() == 2:
                    if self.shape[1] == other.shape[0]:
                        data = []
                        newtype = self.dtype
                        if other.dtype == float:
                            newtype = float
                        for i in range(1, self.shape[0]+1):
                            for j in range(1, other.shape[1]+1):
                                sum = 0
                                for k in range(1, self.shape[1]+1):
                                    sum += self[i, k]*other[k, j]
                                data.append(sum)

                        return BaseArray((self.shape[0], other.shape[1]), newtype, data)
                    else:
                        raise ValueError('Mismatch matrix dimensions!')
                else:
                    raise ValueError('Multiplication only between 2D matrices!')
            else:
                raise ValueError('Mismatch matrix dimensions!')
        else:
            raise ValueError('Wrong type of second parameter!')

    def __truediv__(self, other):
        if type(other) == int or type(other) == float:
            new_data = []
            for i in self.__data:
                new_data.append(i / other)
            return BaseArray(self.shape, float, new_data)
        else:
            raise ValueError('Wrong type of second parameter!')

    def __pow__(self, power, modulo=None):
        retArray = BaseArray(self.shape, self.dtype, self.__data)
        powArray = BaseArray(self.shape, self.dtype, self.__data)
        for i in range(power-1):
            retArray = retArray * powArray
        return retArray

    def __log__(self, base):
        new_data = []
        for i in self.__data:
            new_data.append(math.log(i, base))
        return BaseArray(self.shape, float, new_data)

    def __add__(self, other):
        if type(other) == BaseArray:
            if self.shape == other.shape:
                new_data = []
                new_type = self.dtype
                if other.dtype == float:
                    new_type = float
                for i in range(self.__data.__len__()):
                    new_data.append(self.__data[i] + other.__data[i])
                return BaseArray(self.shape, new_type, new_data)
            else:
                raise ValueError('Mismatch matrix dimensions!')
        else:
            raise ValueError('Wrong type of second parameter!')

    def __sub__(self, other):
        if type(other) == BaseArray:
            if self.shape == other.shape:
                new_data = []
                new_type = self.dtype
                if other.dtype == float:
                    new_type = float
                for i in range(self.__data.__len__()):
                    new_data.append(self.__data[i] - other.__data[i])
                return BaseArray(self.shape, new_type, new_data)
            else:
                raise ValueError('Mismatch matrix dimensions!')
        else:
            raise ValueError('Wrong type of second parameter!')

    def _equals(self, other):
        if type(other) != BaseArray:
            return False
        elif self.shape != other.shape:
            return False
        elif self.dtype != other.dtype:
            return False
        elif self.__data.__len__() != other.__data.__len__():
            return False
        else:
            for i in range(self.__data.__len__()):
                if self.__data[i] != self.__data[i]:
                    return False
        return True


def _check_shape(shape):
    if not isinstance(shape, (tuple, list)):
        raise Exception(f'shape {shape:} is not a tuple or list')
    for dim in shape:
        if not isinstance(dim, int):
            raise Exception(f'shape {shape:} contains a non integer {dim:}')


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if isinstance(inds, slice):
        start, stop, step = inds.start, inds.stop, inds.step
        if start is None:
            start = 1
        if step is None:
            step = 1
        if stop is not None:
            stop += 1
        inds_itt = range(0, s+1)[start:stop:step]
    else:  # type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += (inds[n]-1)*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if not indice:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True
