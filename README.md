# Vaje pri Preizkušanju programske opreme

V tem repositoriju najdete osnovo za vaše sprotne naloge pri predmetu preizkušanje programske opreme. 
V njem se nahaja preprosta implementacija večdimenzionalne tabele *myarray.BaseArray*, za katero boste morali spisati funkcije oz. operacije.

Repositori si klonirajte in ga urejajte kot svoj projekt. 
Med potekom verzioniranja vestno posodabljajte in verzionirajte vaš projekt.
Kasneje boste morali vaše modifikacije aplicirati in popraviti v skladu z novejšo verzijo izvorne knjižnice.

V okviru teh vaj boste morali pripraviti preizkuse in implementacije za naslednje operacije nad *myarray.BaseArray*.

### Izpis

Izpis tabel po vrednosti tako, da so te bolj berljive. Tabelo dimenzij 2x3 z vrednosti od -3 do 3 izpišete kot:

```
-3 -2 -1
 0  1  2
```

Stolpci so ustrezno poravnani.

**Pričakovani preizkusi:** pravilen izpis 2D in 3D tabele, z negativnimi in pozitivnimi celimi števili ter števili z plavajočo vejico.  

### Sortiranje

Sortiranje 1D in 2D tabele z algoritmom [merge sort](https://en.wikipedia.org/wiki/Merge_sort). 
Sortirate ali vrstice ali stolpce tabele, kar izberete preko argumenta v vaši implementaciji.

Primer, tabelo:
```
0  1  1
4  0 -2
1  3  0
```
po vrsticah uredimo v:
```
 0  1  1
-2  0  4
 0  1  3
 ```

po stolpcih pa uredimo v:
```
0  0 -2 
1  1  0
4  3  1
```

**Pričakovani preizkusi:** pravilno sortiranje po vrsticah, pravilno sortiranje po stolpcih, pravilno sortiranje 1D tabele. 

### Iskanje

Poiščite vse pozicije elementa z neko vrednostjo. Pozicije morajo biti podane v koordinatah tabele.
V naslednji 2D tabeli:
```
0 0 0
2 0 0
0 2 2
```
najdemo vrednost 2 na pozicijah:
```
((1, 0), (2, 1), (2, 2))
```

**Pričakovani preizkusi:** pravilno iskanje elementa v 1D, 2D in 3D tabelah, pravilno iskanje kadar je vrednost prisotna večkrat in kadar nikoli ni prisotna.


### Osnovne matematične operacije 

Implementacija osnovnih matematičnih operacij, ki vključuje seštevanje, odštevanje, množenje, deljenje, logaritmiranje in potenciranje (exp funkcija). 

Te operacije imajo različne načine uporabe glede na podane argumente.

Operacije med tabelo in skalarjem izvedejo operacijo nad vsakim elementom tabele v kombinaciji s skalarjem.

Operacije med tabelo in drugo tabelo izvedejo operacije nad istoležečimi elementi. Tu je pomembno, da imata obe tabeli **enake dimenzije**.

Posebna operacija matričnega množenja izvede operacijo matričnega množenja nad dvema 2D tabelama **ustreznih dimenzij**.

Kadar je to potrebno, operacije pretvorijo podatke v tip float (pri operacijah z drugimi float števili, ter operacije kot so deljenje in logaritmiranje), drugače pa ohranijo izvorni tip.

**Pričakovani preizkusi:** pravilno izvajanje vsake operacije nad skalarjem, med 2D in 3D matrikami, ohranjanje ustreznega tipa, pretvorba v float kadar je potrebno, pravilnost matričnega množenja, prejem izjeme ob napačnih dimenzijah.